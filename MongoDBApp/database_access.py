from pymongo import MongoClient
from datetime import datetime
from collections import OrderedDict


class MongoDBAccessor(object):
    database_name = "test_database"

    def __init__(self):
        self.client = MongoClient('mongodb://root:example@0.0.0.0:27017/') 
        self.db = self.client[MongoDBAccessor.database_name] #DB名を設定

    def insert(self, topicName:str, topicContent:OrderedDict):
        
        #self.db.test.insert(post)
        self.db.get_collection(topicName).insert(topicContent)


#mo = MongoSample()
#print(mo.collection_names())
#mo.insert()