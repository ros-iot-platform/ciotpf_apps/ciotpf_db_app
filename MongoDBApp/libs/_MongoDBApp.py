
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from std_msgs.msg import Bool, String, Float32, Int32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Callable,TypeVar, Optional, Any
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _MongoDBAppAppNode(CiotpfAppNodeBase):

    APP_NAME = "mongo_d_b_app"
    NODE_NAME = "mongo_d_b_app_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME, self.APP_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
            
        ]
        self.manual_subscriptions= [
            
        ]
        
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            
            WildCardSubscriptionDataModel("AllTopics", "/ciotpf/*", self.all_topics_subscription_callback, Any)
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            
        ]
        self.manual_publushers: List[ManualPublisherDataModel] = [
            
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)


    # TIMER_PERIODの間隔で勝手に呼ばれる
    def _periodic_task(self) -> None:
        super()._periodic_task()

        
    
        

        
   #全てのトピック 
    def all_topics_subscription_callback(self, topicName: str, message: Any) -> None: 
        raise NotImplementedError() 


        

        

