import rclpy
from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
from libs._MongoDBApp import _MongoDBAppAppNode
from typing import Any
import functools
from rosidl_runtime_py import message_to_ordereddict
from database_access import MongoDBAccessor
from datetime import datetime

class MongoDBAppAppNode(_MongoDBAppAppNode):

    
    


    def __init__(self):
        #親のinitを最初に呼ぶ
        super().__init__()
        ##
        self.dbAccessor = MongoDBAccessor()
    
    
    

    
   #全てのトピック 
    def all_topics_subscription_callback(self, topicName: str, message: Any) -> None: 
        #print(topicName)
        tmp = message_to_ordereddict(message)
        tmp['created_time'] = datetime.utcnow()
        #print(topicName, tmp)
        self.dbAccessor.insert(topicName, tmp)

    



def main(args=None):
    rclpy.init(args=args)

    node = MongoDBAppAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()